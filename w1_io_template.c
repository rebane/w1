#include "w1_io.h"
#include "cortex.h"
#include "stm32g4xx.h"
#include "mask.h"
#include "platform.h"

static void w1_io(uint8_t value);
static uint8_t w1_io_get();
static void w1_io_delay_us(uint32_t us);

void w1_io_init()
{
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
	GPIOA->OTYPER |= GPIO_OTYPER_OT11;
	GPIOA->BSRR = GPIO_BSRR_BS11;
	mask32_set(GPIOA->MODER, GPIO_MODER_MODE11, 1);
	mask32_set(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED11, 3);
}

uint8_t w1_io_reset()
{
	uint8_t answer;

	interrupts_disable();
	w1_io(0);
	w1_io_delay_us(480);
	w1_io(1);
	w1_io_delay_us(70);
	answer = w1_io_get();
	interrupts_enable();
	w1_io_delay_us(420);

	return !answer;
}

uint8_t w1_io_read()
{
	uint8_t result;

	interrupts_disable();
	w1_io(0);
	w1_io_delay_us(6);
	w1_io(1);
	w1_io_delay_us(9);
	result = w1_io_get();
	w1_io_delay_us(55);
	interrupts_enable();

	return result;
}

void w1_io_write(uint8_t bit)
{
	if (bit == 0) {
		interrupts_disable();
		w1_io(0);
		w1_io_delay_us(60);
		w1_io(1);
		w1_io_delay_us(10);
		interrupts_enable();
	} else { 
		interrupts_disable();
		w1_io(0);
		w1_io_delay_us(6);
		w1_io(1);
		w1_io_delay_us(64);
		interrupts_enable();
	}
}

static void w1_io(uint8_t value)
{
	if (value)
		GPIOA->BSRR = GPIO_BSRR_BS11;
	else
		GPIOA->BSRR = GPIO_BSRR_BR11;
}

static uint8_t w1_io_get()
{
	return !!(GPIOA->IDR & GPIO_IDR_ID11);
}

static void w1_io_delay_us(uint32_t us)
{
	SYSTICK->CSR = 0;
	SYSTICK->CVR = 0;
	SYSTICK->RVR = ((PLATFORM_FCPU / 1000000) * us) / 8;
	SYSTICK->CSR = SYSTICK_CSR_ENABLE;
	while (!(SYSTICK->CSR & SYSTICK_CSR_COUNTFLAG));
	SYSTICK->CSR = 0;
}

