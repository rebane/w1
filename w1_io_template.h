#ifndef _W1_IO_H_
#define _W1_IO_H_

#include <stdint.h>

void w1_io_init();
uint8_t w1_io_reset();
uint8_t w1_io_read();
void w1_io_write(uint8_t bit);

#endif

