#ifndef _W1_H_
#define _W1_H_

#include <stdint.h>

#define W1_SKIP_ROM                    0xCC
#define W1_READ_ROM                    0x33
#define W1_MATCH_ROM                   0x55

#define W1_DS18B20_CONVERT_TEMPERATURE 0x44
#define W1_DS18B20_READ_SCRATCHPAD     0xBE
#define W1_DS18B20_READ_POWER          0xB4

#define W1_FAMILY_DS1820               0x10
#define W1_FAMILY_DS2438               0x26
#define W1_FAMILY_DS18B20              0x28

#define w1_has_temperature(romcode)    ((((uint8_t *)(romcode))[0] == W1_FAMILY_DS1820) || (((uint8_t *)(romcode))[0] == W1_FAMILY_DS2438) || (((uint8_t *)(romcode))[0] == W1_FAMILY_DS18B20))
#define w1_has_adc(romcode)            (((uint8_t *)(romcode))[0] == W1_FAMILY_DS2438)
#define w1_has_voltage(romcode)        (((uint8_t *)(romcode))[0] == W1_FAMILY_DS2438)

#define W1_INVALID_TEMPERATURE         32767

typedef struct w1_driver_struct w1_driver_t;

struct w1_driver_struct {
	uint8_t (*reset)();
	uint8_t (*read)();
	void (*write)(uint8_t);
//	uint8_t (*triplet)(uint8_t);
//	void *user;
};

void w1_init(w1_driver_t *driver, uint8_t (*reset)(), uint8_t (*read)(), void (*write)(uint8_t));
void w1_init_triplet(w1_driver_t *driver, uint8_t (*reset)(), uint8_t (*read)(), void (*write)(uint8_t), uint8_t (*triplet)(uint8_t));
uint8_t w1_read_byte(w1_driver_t *driver);
void w1_write_byte(w1_driver_t *driver, uint8_t byte);
uint8_t w1_read_rom(w1_driver_t *driver, void *romcode);
void w1_match_rom(w1_driver_t *driver, void *romcode);
uint8_t w1_search(w1_driver_t *driver, uint8_t max_loops, uint8_t(*callback)(w1_driver_t *, uint8_t *, void *), void *user);

uint8_t w1_crc(uint8_t crc, uint8_t data);

uint8_t w1_ds1820_convert_temperature(w1_driver_t *driver, void *romcode);
uint8_t w1_ds1820_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp);
uint8_t w1_ds2438_convert_temperature(w1_driver_t *driver, void *romcode);
uint8_t w1_ds2438_convert_adc(w1_driver_t *driver, void *romcode, uint8_t adc);
uint8_t w1_ds2438_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp);
uint8_t w1_ds2438_read_adc(w1_driver_t *driver, void *romcode, int16_t *adc);
uint8_t w1_ds18b20_convert_temperature(w1_driver_t *driver, void *romcode);
uint8_t w1_ds18b20_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp);

uint8_t w1_convert_temperature(w1_driver_t *driver, void *romcode);
uint8_t w1_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp);

#endif

