#include "w1.h"

static uint8_t w1_driver_triplet(w1_driver_t *driver, uint8_t bdir);

void w1_init(w1_driver_t *driver, uint8_t (*reset)(), uint8_t (*read)(), void (*write)(uint8_t))
{
	driver->reset = reset;
	driver->read = read;
	driver->write = write;
}

uint8_t w1_read_byte(w1_driver_t *driver)
{
	uint8_t result, i;

	result = 0;

	for (i = 0; i < 8; i++) {
		result |= (driver->read() << i);
	}

	return result;
}

void w1_write_byte(w1_driver_t *driver, uint8_t byte)
{
	uint8_t i;

	for (i = 0; i < 8; i++) {
		driver->write(byte & 0x01);
		byte >>= 1;
	}
}

#ifndef W1_NO_READ_ROM
uint8_t w1_read_rom(w1_driver_t *driver, void *romcode)
{
	unsigned char t[8], crc, i;

	if (!driver->reset())
		return 0;

	w1_write_byte(driver, W1_READ_ROM);
	crc = 0;

	for (i = 0; i < 8; i++) {
		t[i] = w1_read_byte(driver);
		if (i < 7)
			crc = w1_crc(crc, t[i]);
	}

	if (crc == t[7]) {
		for (i = 0; i < 8; i++)
			((uint8_t *)romcode)[i] = t[i];

		return 1;
	}

	return 0;
}
#endif

void w1_match_rom(w1_driver_t *driver, void *romcode)
{
	uint8_t i;

	if (romcode == ((void *)0)) {
		w1_write_byte(driver, W1_SKIP_ROM);
	} else {
		w1_write_byte(driver, W1_MATCH_ROM);
		for (i = 0; i < 8; i++)
			w1_write_byte(driver, ((uint8_t *)romcode)[i]);
	}
}

uint8_t w1_search(w1_driver_t *driver, uint8_t max_loops, uint8_t(*callback)(w1_driver_t *, uint8_t *, void *), void *user)
{
	uint8_t i, crc, last_rn[8], rn[8], last_device, search_bit, desc_bit, triplet_ret, loops, num_found;
	int8_t last_zero;

	triplet_ret = last_device = search_bit = 0;
	loops = 0;
	num_found = 0;
	last_zero = -1;

	for (i = 0; i < 8; i++)
		rn[i] = last_rn[i] = 0;

	desc_bit = 64;

	while (!last_device && (loops++ < max_loops)) {
		if (!driver->reset())
			break;

		w1_write_byte(driver, 0xF0);

		for (i = 0; i < 64; i++) {
			if (i == desc_bit)
				search_bit = 1;
			else if (i > desc_bit)
				search_bit = 0;
			else
				search_bit = (((last_rn[(i / 8)]) >> (i % 8)) & 0x01);

/*			if(driver->triplet != (void *)0){
				triplet_ret = driver->triplet(search_bit);
			}else{*/
				triplet_ret = w1_driver_triplet(driver, search_bit);
//			}
			if ((triplet_ret & 0x03) == 0x03)
				break;

			if (triplet_ret == 0)
				last_zero = i;

			if (triplet_ret & 0x04)
				rn[(i / 8)] |= ((uint8_t)1 << (i % 8));
		}
		if ((triplet_ret & 0x03) != 0x03) {
			if (rn[0]) {
				crc = 0;
				for (i = 0; i < 7; i++)
					crc = w1_crc(crc, rn[i]);

				if (crc == rn[7]) {
					if (callback && !callback(driver, rn, user))
						break;

					num_found++;
				}
			}
			if ((desc_bit == last_zero) || (last_zero < 0))
				last_device = 1;

			desc_bit = last_zero;
		}
		for (i = 0; i < 8; i++) {
			last_rn[i] = rn[i];
			rn[i] = 0;
		}
	}
	return num_found;
}

uint8_t w1_crc(uint8_t crc, uint8_t data)
{
	uint8_t i;

	crc = crc ^ data;

	for (i = 0; i < 8; i++) {
		if (crc & 0x01)
			crc = (crc >> 1) ^ 0x8C;
		else
			crc >>= 1;
	}
	return crc;
}

static uint8_t w1_driver_triplet(w1_driver_t *driver, uint8_t bdir)
{
	uint8_t id_bit, comp_bit, retval;

	id_bit = driver->read();
	comp_bit = driver->read();

	if (id_bit && comp_bit)
		return 0x03;

	if (!id_bit && !comp_bit) {
		retval = bdir ? 0x04 : 0x00;
	} else {
		bdir = id_bit;
		retval = id_bit ? 0x05 : 0x02;
	}
	driver->write(bdir);

	return retval;
}

/**/

uint8_t w1_ds1820_convert_temperature(w1_driver_t *driver, void *romcode)
{
	return w1_ds18b20_convert_temperature(driver, romcode);
}

uint8_t w1_ds1820_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp)
{
	unsigned char t[9], crc, i;
	int16_t t2;

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, W1_DS18B20_READ_SCRATCHPAD);
	crc = 0;

	for (i = 0; i < 9; i++) {
		t[i] = w1_read_byte(driver);
		if (i < 8)
			crc = w1_crc(crc, t[i]);
	}

	if (crc != t[8])
		return 0;

	t2 = ((int16_t)t[0] | ((int16_t)t[1] << 8)) * 50;
	if (t2 == -5500)
		return 0; // (default value -55'C);

	if ((t2 > 15000) || (t2 < (-5000)))
		return 0 ; // temperatuur peab olema <150'C ja >-50'C

	*temp = t2;

	return 1;
}

uint8_t w1_ds2438_convert_temperature(w1_driver_t *driver, void *romcode)
{
	return w1_ds18b20_convert_temperature(driver, romcode);
}

#ifndef W1_NO_DS2438_CONVERT_ADC
uint8_t w1_ds2438_convert_adc(w1_driver_t *driver, void *romcode, uint8_t adc)
{
	if (adc == 0) { // VDD
		if (!driver->reset())
			return 0;

		w1_match_rom(driver, romcode);
		w1_write_byte(driver, 0x4E);
		w1_write_byte(driver, 0x00);
		w1_write_byte(driver, 0x07);
	} else if (adc == 1) {
		if (!driver->reset())
			return 0;

		w1_match_rom(driver, romcode);
		w1_write_byte(driver, 0x4E);
		w1_write_byte(driver, 0x00);
		w1_write_byte(driver, 0x0F);
	} else {
		return 0;
	}

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, 0x48);
	w1_write_byte(driver, 0x00);

	while (!driver->read()); /* loopi ei või jääda? */

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, 0xB4);

	while (!driver->read()); /* loopi ei või jääda? */

	return 1;
}
#endif

uint8_t w1_ds2438_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp)
{
	uint8_t t[9], crc, i;
	int16_t t2;

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, 0xB8);
	w1_write_byte(driver, 0x00);

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, 0xBE);
	w1_write_byte(driver, 0x00);
	crc = 0;

	for (i = 0; i < 9; i++) {
		t[i] = w1_read_byte(driver);
		if (i < 8)
			crc = w1_crc(crc, t[i]);
	}

	if (crc != t[8])
		return 0;

	t2 = ((int16_t)t[1] | ((int16_t)t[2] << 8));
	if (t2 == 0)
		return 0;

	// t2 = ((double)t2 / (double)2.56);
	t2 = ((t2 * 25) / 64);
	if ((t2 > 15000) || (t2 < (-5000)))
		return 0; // temperatuur peab olema <150'C ja >-50'C

	*temp = t2;

	return 1;
}

#ifndef W1_NO_DS2438_READ_ADC
uint8_t w1_ds2438_read_adc(w1_driver_t *driver, void *romcode, int16_t *adc)
{
	uint8_t t[9], crc, i;
	int16_t t2;

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, 0xB8);
	w1_write_byte(driver, 0x00);

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, 0xBE);
	w1_write_byte(driver, 0x00);
	crc = 0;

	for (i = 0; i < 9; i++) {
		t[i] = w1_read_byte(driver);
		if (i < 8)
			crc = w1_crc(crc, t[i]);
	}

	if (crc != t[8])
		return 0;

	t2 = (uint16_t)t[3] | ((uint16_t)t[4] << 8);
	if (t2 < 0)
		t2 = 0;

	if (t2 > 499)
		return 0;

//	if(t2 > 1023)
//		t2 = 1023;

	*adc = t2;

	return 1;
}
#endif

uint8_t w1_ds18b20_convert_temperature(w1_driver_t *driver, void *romcode)
{
	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, W1_DS18B20_CONVERT_TEMPERATURE);
//	while(!driver->read()); /* loopi ei või jääda? */

	return 1;
}

uint8_t w1_ds18b20_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp)
{
	uint8_t t[9], crc, i;
	int16_t t2;

	if (!driver->reset())
		return 0;

	w1_match_rom(driver, romcode);
	w1_write_byte(driver, W1_DS18B20_READ_SCRATCHPAD);
	crc = 0;

	for (i = 0; i < 9; i++) {
		t[i] = w1_read_byte(driver);
		if (i < 8)
			crc = w1_crc(crc, t[i]);
	}

	if (crc != t[8])
		return 0;

	t2 = (int16_t)t[0] | ((int16_t)t[1] << 8);

	if (t2 == 1360)
		return 0; // (default value +85'C);

	// t2 = ((double)t2 * (double)6.25);
	t2 = ((t2 * 25) / 4);

	if ((t2 > 15000) || (t2 < (-5000)))
		return 0 ; // temperatuur peab olema <150'C ja >-50'C

	*temp = t2;

	return 1;
}

uint8_t w1_convert_temperature(w1_driver_t *driver, void *romcode)
{
	if (((uint8_t *)romcode)[0] == W1_FAMILY_DS1820)
		return w1_ds1820_convert_temperature(driver, romcode);
	else if (((uint8_t *)romcode)[0] == W1_FAMILY_DS2438)
		return w1_ds2438_convert_temperature(driver, romcode);
	else if(((uint8_t *)romcode)[0] == W1_FAMILY_DS18B20)
		return w1_ds18b20_convert_temperature(driver, romcode);

	return 0;
}

uint8_t w1_read_temperature(w1_driver_t *driver, void *romcode, int16_t *temp)
{
	if (((uint8_t *)romcode)[0] == W1_FAMILY_DS1820)
		return w1_ds1820_read_temperature(driver, romcode, temp);
	else if (((uint8_t *)romcode)[0] == W1_FAMILY_DS2438)
		return w1_ds2438_read_temperature(driver, romcode, temp);
	else if (((uint8_t *)romcode)[0] == W1_FAMILY_DS18B20)
		return w1_ds18b20_read_temperature(driver, romcode, temp);

	return 0;
}

